# Listen
Aufgabe Listen für Hochsprachenprogrammierung / Mecke an der Jade Hochschule

Aufgabe: Fügen sie ein Liste in die Methode "getList" ein und fügen sie ihr drei strings hinzu nämlich die Namen:
- Daniel
- Tim
- Peter


## Getting Started
* Clonen Sie die Repository auf Ihren Rechner
* Öffnen Sie die Solution *.sln in Visual Studio
* Führen Sie die Implementierung in der Methode durch

Wenn Sie fertig sind, dann
* Im Projektmappen Explorer (engl. Solution Explorer) klicken Sie rechts das -Unittesting Projekt und wählen Sie "Tests ausführen"
* Starten Sie die Tests innerhalb des Test-Explorers

Diesen Ablauf können Sie auch mit Hilfe des Videos "Aufgabensammlung Einstieg" nachvollziehen
