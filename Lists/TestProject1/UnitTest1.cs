using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lists;
using System.Collections.Generic;

namespace TestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            //Arrange
            ListenControl prg = new ListenControl();

            //Act
            List<string> result = prg.getList();


            List<string> Studenten = new List<string>();
            Studenten.Add("Daniel");
            Studenten.Add("Tim");
            Studenten.Add("Peter");

            //Assert
            CollectionAssert.AreEqual(result, Studenten);
        }
    }
}
