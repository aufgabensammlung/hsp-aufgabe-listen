﻿using System;
using System.Collections.Generic;

namespace Lists
{
   public class ListenControl
    {
        // ToDo1: Erzeugen Sie hier das Objekt einer Liste
        // als Eigenschaft der Klasse ListenControl
        // Achten Sie auf
        // - Sichtbarkeit
        // - den Datentyp der Liste, List<DatenTyp>
        // - den Namen und 
        // - die Zuweisung mit der Instanziierung

        // ToDo2: Fügen Sie mit .Add die einzelnen Namen
        // (siehe Aufgabenstellung) in die Liste ein, z.B im Konstruktor


        public List<string> getList()
        {
            // ToDo3: Geben Sie hier diese richtige Liste zurück:
            return new List<string>();
        }

        static void Main(string[] args)
        {
            ListenControl prg = new ListenControl();
            prg.getList();
        }

    }
}
